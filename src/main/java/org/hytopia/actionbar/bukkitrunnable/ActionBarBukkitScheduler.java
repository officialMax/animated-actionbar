package org.hytopia.actionbar.bukkitrunnable;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.hytopia.actionbar.ActionBarPlugin;

public class ActionBarBukkitScheduler extends BukkitRunnable {

  private int currentRow;
  private int switchDelay;

  @Override
  public void run() {
    if (Bukkit.getOnlinePlayers().isEmpty())
      return;

    switchDelay++;
    if (switchDelay >= ActionBarPlugin.getInstance().getSwitchDelay()) {
      currentRow++;
      switchDelay = 0;
    }
    if (currentRow >= ActionBarPlugin.getInstance().getRows().size()) {
      switchDelay = 0;
      currentRow = 0;
    }

    for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
      onlinePlayer.spigot().sendMessage(ChatMessageType.ACTION_BAR, getMessage(onlinePlayer));
    }
  }

  public TextComponent getMessage(Player player) {
    String message = ActionBarPlugin.getInstance().getRows().get(currentRow);
    message = message.
            replace("%prefix%", ActionBarPlugin.getInstance().getPrefix()).
            replace("%player_name%", player.getName()).
            replace("%world_name%", player.getWorld().getName()).
            replace("%location_x%", String.valueOf(player.getLocation().getBlockX())).
            replace("%location_y%", String.valueOf(player.getLocation().getBlockY())).
            replace("%location_z%", String.valueOf(player.getLocation().getBlockZ())).
            replace("%player_health%", String.valueOf(player.getHealth())).
            replace("%player_foodlevel%", String.valueOf(player.getFoodLevel())).
            replace("&", "§");

    return new TextComponent(message);
  }
}
