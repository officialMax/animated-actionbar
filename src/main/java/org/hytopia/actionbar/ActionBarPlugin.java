package org.hytopia.actionbar;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.hytopia.actionbar.bukkitrunnable.ActionBarBukkitScheduler;
import org.hytopia.actionbar.command.ActionBarCommand;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class ActionBarPlugin extends JavaPlugin {

  private static ActionBarPlugin instance;

  private String prefix;
  private int refreshDelay;
  private int switchDelay;
  private List<String> rows;

  private int bukkitScheduler;

  private final File file = new File(getDataFolder(), "config.yml");
  private YamlConfiguration yamlConfiguration;

  @Override
  public void onEnable() {
    instance = this;
    loadPlugin();
    getCommand("actionbar").setExecutor(new ActionBarCommand());
    super.onEnable();
  }

  @Override
  public void onDisable() {
    super.onDisable();
  }

  public void loadPlugin() {
    createFile();
    BukkitScheduler scheduler = Bukkit.getScheduler();
    if (scheduler.isCurrentlyRunning(bukkitScheduler) || scheduler.isQueued(bukkitScheduler))
      scheduler.cancelTask(bukkitScheduler);

    bukkitScheduler = scheduler.scheduleAsyncRepeatingTask(this, new ActionBarBukkitScheduler(), refreshDelay, refreshDelay);
    createFile();
  }

  private void createFile() {
    // create folder if isn't existing
    getDataFolder().mkdir();
    try {
      // create and load file if isn't existing
      if (!file.exists()) {
        file.createNewFile();
        yamlConfiguration = YamlConfiguration.loadConfiguration(file);

        yamlConfiguration.set("prefix", "&6&lActionBar &8» &7");
        yamlConfiguration.set("refreshDelay", 20);
        yamlConfiguration.set("switchDelay", 2);
        yamlConfiguration.set("rows", Arrays.asList("&8» &7Thanks", "&8» &7for", "&8» &7using", "&8» &7my", "&8» &7plugin"));

        yamlConfiguration.save(file);
        return;
      }

    } catch (IOException e) {
      e.printStackTrace();
    }

    // load yml config
    yamlConfiguration = YamlConfiguration.loadConfiguration(file);

    // set variable objects
    prefix = Objects.requireNonNull(yamlConfiguration.getString("prefix")).replace("&", "§");
    refreshDelay = yamlConfiguration.getInt("refreshDelay");
    switchDelay = yamlConfiguration.getInt("switchDelay");
    rows = yamlConfiguration.getStringList("rows");
  }

  public static ActionBarPlugin getInstance() {
    return instance;
  }

  public String getPrefix() {
    return prefix;
  }

  public int getSwitchDelay() {
    return switchDelay;
  }

  public List<String> getRows() {
    return rows;
  }
}
