package org.hytopia.actionbar.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.hytopia.actionbar.ActionBarPlugin;

public class ActionBarCommand implements CommandExecutor {
  @Override
  public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
    if (commandSender instanceof Player) {
      Player player = ((Player) commandSender);
      if (player.hasPermission("actionbar.reload")) {
        if (strings.length == 1) {
          if (strings[0].equalsIgnoreCase("reload")) {
            // plugin refresh
            ActionBarPlugin.getInstance().loadPlugin();
            player.sendMessage(ActionBarPlugin.getInstance().getPrefix() + "Reloaded config");
            player.sendMessage(ActionBarPlugin.getInstance().getPrefix() + "Restarted scheduler");
          } else {
            // command syntax
            player.sendMessage(ActionBarPlugin.getInstance().getPrefix() + "/actionbar <reload>");
          }
        } else {
          // command syntax
          player.sendMessage(ActionBarPlugin.getInstance().getPrefix() + "/actionbar <reload>");
        }
      }
    } else {
      if (strings.length == 1) {
        if (strings[0].equalsIgnoreCase("reload")) {
          // plugin refresh
          ActionBarPlugin.getInstance().loadPlugin();
          commandSender.sendMessage("Reloaded config");
          commandSender.sendMessage("Restarted scheduler");
        } else {
          // command syntax
          commandSender.sendMessage("actionbar <reload>");
        }
      } else {
        // command syntax
        commandSender.sendMessage("actionbar <reload>");
      }
    }
    return false;
  }
}
